# Poznámkový blok

## Z Progtestu:
### Poznámkový blok

Vytvořte program pro správu poznámek.

Implementujte následující funkcionalitu:

* reprezentace poznámek v plaintextu a 1 dalším formátu (HTML, Markdown, AsciiDoc, ...)
* uživatelské rozhraní, které bude zobrazovat formátované poznámky
* systém štítků (tagů) a kategorií poznámek
* vyhledávání podle textu, tagů, kategorií ...
* exportování a importování poznámek (všech, podmnožiny na základě vyhledávání) - vytvořte vhodný formát

Použití polymorfismu (doporučené)

* druhy poznámek: složka poznámek, textová poznámka, seznam úkolů, nákupní seznam (zobrazuje celkovou cenu)
* filtry (skládání k vyhledávání: (ne)obsahuje text, a zároveň, nebo, procento úkolů splněno
* různé druhy řazení poznámek
* různé formáty či zobrazení

Doplňující informace:

https://guides.github.com/features/mastering-markdown/

Volitelně: Šifrování poznámek

## Vlastní zpracování:
#### Popis aplikace:
Jednoduchá aplikace na práci s poznámkami ve formátu plaintext a markdown. Aplikace je naprogramována s použitím
knihovny Ncurses a obsahuje i jednoduché UI, skládající se převážně z různých menu. Aplikace obsahuje jednoduchý 
textový editor, který podporuje základní markdown formátování, např. text zapsaný mezi * se zobrazuje napsaný kurzívou.
Vzhledem k tomu, že terminálové zobrazení neumožňuje změnu velikosti písma, nadpisy jsou naznačeny pouze barevně. 
K vytvořeným poznámkám může uživatel přiřadit kategorii (prostřednictvím UI)
nebo tag (na poslední řádek napíše !tags: a b c (takto budou přiřazeny tagy "a" "b" "c")).
Aplikace dále umožňuje vyhledávání poznámek podle textu, kategorií a tagů (prostřednictvím UI).
Aplikace umožňuje poznámky ukládat jako klasické soubory do složky, kterou si sama vytvoří. Nejjednodušší forma exportu z 
aplikace je tak prosté přesunutí/zkopírování požadovaného souboru z této složky. Aplikace ale umožňuje i export
na základě kategorií, tagů nebo textu pomocí UI (implementováno jako multichoice menu). Aplikace zvolené
soubory kopíruje do CWD (složka ve které je spouštěcí soubor aplikace).
Importování je opět možné přes UI z CWD.

#### Použití polymorfismu:
Třída CTextEditor pracuje s abstraktní třídou CFormat, pomocí které je zpracováno formátování textu a ukládání
souboru. Tato třída má dvě public metody:
    1) setFormat() - tato metoda zpracovává formátovaní. Vstupními argumenty jsou CTextStorage (ve které je uložný) aktuální
    text dokumentu a CWindow odpovídající oknu editoru. Tato metoda poté aplikuje přímo "do okna" formátování. Pro markdown
    například obarví řádky začínající "# " (nadpisy) na červeno. 
    2) getFileExt() - tato metoda vrací koncovku souboru daného formátu, například CMarkdown vrací ".md". Aplikace podle 
    této informace ukládá danou poznámku.
    3) needsColors() - tato metoda vrací true, pokud je dané formátování závislé na schopnosti terminálu zobrazovat barvy.
    
    
    
### Memory leaky, Valgrind
Knihovna Ncurses způsobuje velké množství problémů s paměťí, převážně se jedná o neuvolňování paměti a neinicializování
některých vnitřních proměnných. V programu jsou využívány všechny dostupné funkce na uvolňování použitých paměťových 
prostředků prostředků.
Pro spuštění programu s Valgrindem je potřeba použít tento příkaz:
    "valgrind --leak-check=full --show-leak-kinds=all --log-file=ValgrindLog ./kuchejak" (= make valgrind)
        (výstup je přesměrován do souboru _ValgrindLog_, protože při přerušení běhu aplikace by ncurses mód terminálu
        nebyl správně ukončen, což by mohlo vést k různým problémům - pravděpodobně by bylo potřeba terminál restartovat)

Z https://invisible-island.net/ncurses/ncurses.faq.html#config_leaks:
>    Perhaps you used a tool such as dmalloc or valgrind to check for memory leaks. It will normally report 
>    a lot of memory still in use. That is normal.
>    ...
>    Any implementation of curses must not free the memory associated with a screen, since (even after calling endwin()),
>    it must be available for use in the next call to refresh(). There are also chunks of memory held for performance reasons...
